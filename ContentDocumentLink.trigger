trigger trig_ContentDocumentLink_AfterInsert on ContentDocumentLink (after insert) {
   if(Trigger.isAfter && Trigger.isInsert){
      
      TW_ContentDocumentLink_Handler.onAfterInsert(Trigger.New);  
    }
}