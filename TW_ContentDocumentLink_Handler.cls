/* 
Class : TW_ContentDocumentLink_Handler
Description: Called from trigger - 'trig_ContentDocumentLink_AfterInsert', after insert case.   
in Banking type account (Record Type) Restrict pdf files upload through 'Upload Files' button present in Notes and Attachments  
Owner : gandla.akhil@mtxb2b.com
*/

public class TW_ContentDocumentLink_Handler {
    //This method restrict .pdf file upload to 'Banking' type account records
    public static void onAfterInsert(list<ContentDocumentLink> lstCntLinks) {
       String strObjPrefix; // to check parent record prefix value like for Account object: 001, for Opportunity: 006 
       Set<Id> setCntDocIds = new set<Id>();
       set<Id> setAgmtIds = new set<Id>();//Store Parent record i.e. Agreement Ids
       map<Id, Account> mapAgmt;
       try{
           for(ContentDocumentLink clIterator : lstCntLinks) {
               strObjPrefix = String.valueOf(clIterator.LinkedEntityId).substring(0, 3); // Return first letters of record id like 001 for Account 
               if(strObjPrefix == Account.sObjectType.getDescribe().getKeyPrefix()) {
                   setCntDocIds.add(clIterator.ContentDocumentId);// Content Document Id
                   setAgmtIds.add(clIterator.LinkedEntityId);// Agreement Id - Parent record Id
               }
           }
           Id recordTypeIdBanking = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Banking').getRecordTypeId();// Account object- 'Banking' Reord Type id
           if(setCntDocIds.size() > 0 && setAgmtIds.size() > 0 ) {           
               mapAgmt = new map<Id, Account>([SELECT Id, Name,RecordTypeId FROM Account WHERE Id IN :setAgmtIds and RecordTypeId = :recordTypeIdBanking]);
               //Fetching parent record details and restricting only if Record type is Account 
               if(mapAgmt.size() > 0){
                   map<Id, ContentDocument> mapContentDocuments = new map<Id, ContentDocument>([SELECT Id, Title, FileExtension FROM ContentDocument WHERE Id IN :setCntDocIds]);
                   list<ContentDocument> lstCntDocsToUpdate = new list<ContentDocument>();        
                   for(ContentDocumentLink cdlIterator : lstCntLinks) {
                       ContentDocument objCntDoc = mapContentDocuments.get(cdlIterator.ContentDocumentId);
                       // Allow all files except : pdf 
                       if(objCntDoc.FileExtension == 'pdf'){
                           cdlIterator.addError('You can not upload pdf files for Banking type accounts.');//Showing error   
                           // This line will abort file upload option. 
                           // Error message will be displayed only when 'ContentDocumentLink' object has some RecordType created (any other than Master)
                           // else Generic message will be dispayed - 'Can not add 1 file to Account'.   
                       }                        
                   }  
               }
           }
       }
       Catch (Exception ex){
           system.debug('Exception in TW_ContentDocumentLink_Handler class :' + ex.getMessage());
       }
   }
}